<?php

namespace frontend\controllers;

use common\models\Order;
use yii\web\Controller;

/**
 * Orders controller
 */
class OrdersController extends Controller
{

    /**
     * Displays index orders page
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'orders' => Order::getAll(),
        ]);
    }

}
