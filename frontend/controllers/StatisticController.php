<?php

namespace frontend\controllers;

use common\forms\OrderSearchForm;
use common\models\statistic\Statistic;
use common\models\Zip;
use Yii;
use yii\web\Controller;

/**
 * Statistic controller
 */
class StatisticController extends Controller
{
    const LIST_TYPE_REGION = 'region';
    const LIST_TYPE_MONTH = 'month';

    /**
     * Displays index statistic page
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $type = Yii::$app->request->post('type', self::LIST_TYPE_MONTH);

        $searchForm = new OrderSearchForm();
        $searchForm->load(Yii::$app->request->post(), '');
        if (!$searchForm->validate()) {
            Yii::$app->session->setFlash('error', $searchForm->errors);
        }

        if ($type == self::LIST_TYPE_REGION) {
            $items = Statistic::getByRegion($searchForm);
        } else {
            $items = Statistic::getByMonth($searchForm);
        }

        $regions = [0 => ''];
        $zips = Zip::getAll();
        foreach ($zips as $zip) {
            $regions[$zip->zip] = $zip->region;
        }

        $monthes = [
            '0'  => '',
            '1'  => 'January',
            '2'  => 'Febrary',
            '3'  => 'March',
            '4'  => 'April',
            '5'  => 'May',
            '6'  => 'June',
            '7'  => 'Jule',
            '8'  => 'August',
            '9'  => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        return $this->render('index', [
            'type'    => $type,
            'types'   => static::getTypes(),
            'items'   => $items,
            'region'  => Yii::$app->request->post('zip'),
            'month'   => Yii::$app->request->post('month'),
            'regions' => $regions,
            'monthes' => $monthes,
        ]);
    }

    protected static function getTypes()
    {
        return [
            self::LIST_TYPE_REGION => 'По регионам',
            self::LIST_TYPE_MONTH  => 'По месяцам',
        ];
    }
}
