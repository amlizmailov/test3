<?php
/** @var array $regions */

?>

<div class="row">

    <table class="table">
        <thead>
        <tr>
            <td>Индекс</td>
            <td>Регион</td>
            <td>Макс. время доставки</td>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($regions as $region) : ?>
            <tr>
                <td><?= $region['zip'] ?></td>
                <td><?= $region['region'] ?></td>
                <td><?= $region['days_max'] ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

</div>
