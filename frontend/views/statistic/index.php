<?php

/** @var string $type */
/** @var int $region */
/** @var string $month */

/** @var array $types */

/** @var array $regions */

/** @var array $monthes */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="row">

    <?php $form = ActiveForm::begin(['id' => 'items-form']) ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <table class="table">
        <tr>
            <td>Тип статистики</td>
            <td>Регион</td>
            <td>Месяц</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <?php echo Html::dropDownList('type', $type, $types, ['class' => 'form-control']) ?>
            </td>
            <td>
                <?php echo Html::dropDownList('zip', $region, $regions, ['class' => 'form-control']) ?>
            </td>
            <td>
                <?php echo Html::dropDownList('month', $month, $monthes, ['class' => 'form-control']) ?>
            </td>
            <td>
                <?php echo Html::submitButton('Send', ['class' => 'form-control btn btn-primary']) ?>
            </td>
        </tr>
    </table>


    <?php ActiveForm::end() ?>

    <table class="table">
        <thead>
        <tr>
            <th><?= ($type == 'region') ? 'Регион' : 'Месяц' ?></th>
            <th>Общее количество</th>
            <th>Кол-во заказов, доставленных вовремя</th>
            <th>% заказов, доставленных вовремя</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= ($type == 'region') ? $item['region'] : $item['monthname'] ?></td>
                <td><?= $item['total_count'] ?></td>
                <td><?= $item['delivered_count'] ?></td>
                <td><?= number_format($item['delivered_count'] / ($item['total_count'] / 100), 0) ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

</div>
