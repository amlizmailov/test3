<?php

/** @var \common\models\Order $orders */

?>

<div class="row">


    <table class="table">
        <thead>
        <tr>
            <th>№</th>
            <th>Получатель</th>
            <td>Дата отправки</td>
            <td>Дата прибытия</td>
            <th>Город</th>
            <th>Адрес</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($orders as $order) : ?>
            <?php /** @var \common\models\Order $order */ ?>
            <tr>
                <td><?= $order->order_id ?></td>
                <td><?= $order->recepient ?></td>
                <td><?= $order->orderDate->date_shipped ?></td>
                <td><?= $order->orderDate->date_delivered ?></td>
                <td><?= $order->zipobj->region ?></td>
                <td><?= $order->address ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

</div>
