<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_dates}}`.
 */
class m190422_104738_create_orders_dates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_dates}}', [
            'order_id'       => $this->integer(11)->comment('идентификатор заказа'),
            'date_created'   => $this->dateTime()->comment('дата создания заказа'),
            'date_shipped'   => $this->dateTime()->comment('дата отгрузки заказа'),
            'date_delivered' => $this->dateTime()->comment('дата доставки заказа'),
        ]);

        $this->addForeignKey('frgn_order_id_orders', 'orders_dates', 'order_id', 'orders', 'order_id', 'CASCADE',
            'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders_dates}}');
    }
}
