<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zips}}`.
 */
class m190422_090654_create_zips_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zips}}', [
            'zip'      => $this->primaryKey(),
            'region'   => $this->string(300)->comment('регион, которому принадлежит индекс'),
            'days_max' => $this->integer(3)->unsigned()->comment('контрольный срок доставки (максимальное число дней)'),
        ]);

        $this->createIndex('days_max_idx', 'zips', 'days_max');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%zips}}');
    }
}
