<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m190422_102600_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'order_id'  => $this->primaryKey(),
            'recepient' => $this->string(200)->comment('ФИО получателя'),
            'zip'       => $this->integer(11)->comment('почтовый индекс адреса получателя'),
            'address'   => $this->string(300)->comment('адрес получателя'),
        ]);

        $this->addForeignKey('frgn_zip_zips', 'orders', 'zip', 'zips', 'zip', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
