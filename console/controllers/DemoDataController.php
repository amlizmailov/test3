<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Zip;
use common\models\Order;
use common\models\OrderDate;

class DemoDataController extends Controller
{

    /**
     * заполнение данными таблицы zips
     */
    public function actionFillZipTable()
    {
        $zips = [
            ['163000', 'Архангельск', 2],
            ['414000', 'Астрахань', 7],
            ['241001', 'Брянск', 12],
            ['690000', 'Владивосток', 12],
        ];

        foreach ($zips as $zip) {
            $zipModel = new Zip();
            $zipModel->zip = $zip[0];
            $zipModel->region = $zip[1];
            $zipModel->days_max = $zip[2];

            if (!$zipModel->save()) {
                Yii::error('cannot save ZipModel: ' . $zip[0]);
                var_dump($zipModel->errors);
            }
        }
    }

    /**
     * заполнение данными таблицы orders и orders_dates
     */
    public function actionFillOrdersTable()
    {
        $orders = [
            ['1215', 'Иванов Петр Иванович', 163000, 'ул.Ленина 12'],
            ['2029', 'Борисов Илья Васильевич', 163000, 'ул.Чернышевского 21'],
            ['40102', 'Гоголь Николай', 414000, 'пр. Петра 1'],
            ['1234', 'Трубецкой Алексей', 241001, 'ул.Магистральная 124'],
        ];

        foreach ($orders as $order) {
            $orderModel = new Order();
            $orderModel->order_id = $order[0];
            $orderModel->recepient = $order[1];
            $orderModel->zip = $order[2];
            $orderModel->address = $order[3];

            $transaction = Yii::$app->db->beginTransaction();

            if (!$orderModel->save()) {
                Yii::error('cannot save ZipModel: ' . $order[0]);
                var_dump($orderModel->errors);
                $transaction->rollBack();
            }

            $dateNow = new \DateTime('now');
            // рандомно получаем кол-во дней - либо доставка успевает, либо нет
            $days = mt_rand(1, $orderModel->zipobj->days_max + 5);
            $interval = date_interval_create_from_date_string($days . ' days');
            $dateDelivery = clone $dateNow;
            $dateDelivery->add($interval);
            
            $orderDateModel = new OrderDate();
            $orderDateModel->order_id = $orderModel->order_id;
            $orderDateModel->date_created = $dateNow->format('Y-m-d');
            $orderDateModel->date_shipped = $dateNow->format('Y-m-d');
            $orderDateModel->date_delivered = $dateDelivery->format('Y-m-d');
            if (!$orderDateModel->save()) {
                Yii::error('cannot save OrderModel: ' . $order[0]);
                var_dump($orderDateModel->errors);
                $transaction->rollBack();
            }

            $transaction->commit();
        }
    }


}