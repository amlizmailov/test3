Данный проект является тестовым заданием.

Скопируйте себе репозиторий git clone

В консоли перейдите в папку проекта и последовательно выполняйте команды:

- php init 
- composer update
- php yii migrate (перед этим не забудьте создать БД и настройкии прописать в файл common/config/main-local.php)
- php yii demo-data/fill-zip-table (для заполнения таблицы с регионами)
- php yii demo-data/fill-orders-table (для заполнения таблицы с заказами)