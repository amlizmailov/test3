<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class OrderDate
 *
 * @property int $order_id
 * @property string $date_created
 * @property string $date_shipped
 * @property string $date_delivered
 * @property Order $order
 *
 * @package common\models
 */
class OrderDate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders_dates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'required'],
            [['date_created', 'date_shipped', 'date_delivered'], 'string'],
            ['order_id', 'exist', 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'       => 'ID заказа',
            'date_created'   => 'дата создания заказа',
            'date_shipped'   => 'дата отгрузки заказа',
            'date_delivered' => 'дата доставки заказа',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::class, ['order_id' => 'order_id']);
    }

    /**
     * @return Order[]|[]
     */
    public static function getAll()
    {
        return self::find()->all();
    }

    /**
     * @param $order_id
     * @return Order|null
     */
    public static function getById($order_id)
    {
        return self::find()->where(['order_id' => $order_id])->one();
    }

}