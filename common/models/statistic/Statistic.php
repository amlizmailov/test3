<?php

namespace common\models\statistic;

use common\forms\OrderSearchForm;
use common\models\Order;
use common\models\OrderDate;
use common\models\Zip;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

class Statistic extends Model
{
    /**
     * статистика по регионам
     *
     * @param OrderSearchForm|null $searchForm
     * @return ActiveRecord[]|[]
     */
    public static function getByRegion(OrderSearchForm $searchForm = null)
    {
        $query = new Query();

        $query->select([
            'region'          => 'zips.region',
            'total_count'     => 'count(orders.order_id)',
            'delivered_count' => new Expression('sum(if(zips.days_max >= datediff(orders_dates.date_delivered, orders_dates.date_shipped),1,0))'),
        ])
            ->from(Order::tableName())
            ->leftJoin(Zip::tableName(), Order::tableName() . '.zip = ' . Zip::tableName() . '.zip')
            ->leftJoin(OrderDate::tableName(),
                Order::tableName() . '.order_id = ' . OrderDate::tableName() . '.order_id')
            ->groupBy(Order::tableName() . '.zip');

        if ($searchForm->zip) {
            $query->andWhere([Order::tableName() . '.zip' => $searchForm->zip]);
        }

        return $query->all();
    }

    /**
     * статистика по месяцам
     *
     * @param OrderSearchForm|null $searchForm
     * @return ActiveRecord[]|[]
     */
    public static function getByMonth(OrderSearchForm $searchForm = null)
    {
        $query = new Query();

        $query->select([
            'monthname'       => 'monthname(orders_dates.date_delivered)',
            'month'           => 'month(orders_dates.date_delivered)',
            'total_count'     => 'count(orders.order_id)',
            'delivered_count' => new Expression('sum(if(zips.days_max >= datediff(orders_dates.date_delivered, orders_dates.date_shipped),1,0))'),
        ])
            ->from(Order::tableName())
            ->leftJoin(Zip::tableName(), Order::tableName() . '.zip = ' . Zip::tableName() . '.zip')
            ->leftJoin(OrderDate::tableName(),
                Order::tableName() . '.order_id = ' . OrderDate::tableName() . '.order_id')
            ->groupBy('month');

        if ($searchForm->zip) {
            $query->andWhere([Order::tableName() . '.zip' => $searchForm->zip]);
        }

        if ($searchForm->month) {
            $query->andWhere(new Expression('month(orders_dates.date_delivered) = ' . $searchForm->month));
        }

        return $query->all();
    }

}