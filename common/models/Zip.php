<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Zip
 *
 * @property int $zip
 * @property string $region
 * @property int $days_max
 *
 * @package common\models
 */
class Zip extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%zips}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region', 'days_max'], 'required'],
            [['region'], 'string', 'max' => 200],
            [['days_max'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zip'      => 'Индекс',
            'region'   => 'Регион',
            'days_max' => 'Макс. срок доставки',
        ];
    }

    /**
     * @return Zip[]|[]
     */
    public static function getAll()
    {
        return self::find()->all();
    }

    /**
     * @param $zip
     * @return Zip|null
     */
    public static function getById($zip)
    {
        return self::find()->where(['zip' => $zip])->one();
    }

}