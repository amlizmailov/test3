<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use common\forms\OrderSearchForm;

/**
 * Class Order
 *
 * @property int $order_id
 * @property string $recepient
 * @property int $zip
 * @property string $address
 * @property Zip $zipobj
 * @property OrderDate $orderDate
 *
 * @package common\models
 */
class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recepient', 'zip', 'address'], 'required'],
            [['recepient'], 'string', 'max' => 200],
            [['address'], 'string', 'max' => 300],
            ['zip', 'exist', 'targetClass' => Zip::class, 'targetAttribute' => ['zip' => 'zip']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'  => 'ID заказа',
            'recepient' => 'ФИО получателя',
            'zip'       => 'Регион',
            'address'   => 'Макс. срок доставки',
        ];
    }

    public function getZipobj()
    {
        return $this->hasOne(Zip::class, ['zip' => 'zip']);
    }

    public function getOrderDate()
    {
        return $this->hasOne(OrderDate::class, ['order_id' => 'order_id']);
    }

    /**
     * @return Order[]|[]
     */
    public static function getAll(OrderSearchForm $searchForm = null)
    {
        return static::getAllQuery($searchForm)
            ->joinWith('orderDate')
            ->joinWith('zipobj')
            ->all();
    }

    /**
     * @return int
     */
    public static function getAllCount(OrderSearchForm $searchForm = null)
    {
        return static::getAllQuery($searchForm)->count();
    }

    /**
     * общая часть запроса на получение всех заказов
     *
     * @param OrderSearchForm|null $searchForm
     * @return \yii\db\ActiveQuery
     */
    public static function getAllQuery(OrderSearchForm $searchForm = null)
    {
        $query = self::find();

        if ($searchForm->order_id) {
            $query->andWhere([Order::tableName() . '.order_id' => $searchForm->order_id]);
        }

        if ($searchForm->zip) {
            $query->andWhere([Order::tableName() . '.zip' => $searchForm->zip]);
        }

        return $query;
    }

    /**
     * @param $order_id
     * @return Order|null
     */
    public static function getById($order_id)
    {
        return self::find()->where(['order_id' => $order_id])->one();
    }

    /**
     * вернуть непросроченные заказы
     *
     * @param OrderSearchForm|null $searchForm
     * @return Order[]|[]
     */
    public static function getNotExpiredOrders(OrderSearchForm $searchForm = null)
    {
        return static::getNotExpiredOrdersQuery($searchForm)->all();
    }

    /**
     * вернуть кол-во непросроченныых заказов
     *
     * @param OrderSearchForm|null $searchForm
     * @return int
     */
    public static function getNotExpiredOrdersCount(OrderSearchForm $searchForm = null)
    {
        return static::getNotExpiredOrdersQuery($searchForm)->count();
    }

    /**
     * @param OrderSearchForm|null $searchForm
     * @return \yii\db\ActiveQuery
     */
    protected static function getNotExpiredOrdersQuery(OrderSearchForm $searchForm = null)
    {
        $query = self::find()
            ->leftJoin(OrderDate::tableName(),
                Order::tableName() . '.order_id = ' . OrderDate::tableName() . '.order_id')
            ->leftJoin(Zip::tableName(), Order::tableName() . '.zip = ' . Zip::tableName() . '.zip')
            ->where(new Expression(Zip::tableName() . '.days_max >= datediff(orders_dates.date_delivered, orders_dates.date_shipped)'));

        if ($searchForm->zip) {
            $query->andWhere([Zip::tableName() . '.zip' => $searchForm->zip]);
        }

        return $query;
    }

}