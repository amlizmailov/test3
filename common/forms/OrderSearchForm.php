<?php

namespace common\forms;

use yii\base\Model;

class OrderSearchForm extends Model
{
    public $order_id;
    public $zip;
    public $month;

    public function rules()
    {
        return [
            [['order_id', 'zip', 'month'], 'integer'],
        ];
    }

}